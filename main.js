const electron = require('electron');
const path = require('path')
const screenshot = require('screenshot-desktop')

//自动更新
var tray; //TODO
//初始化默认应用窗口菜单快捷键


//初始化托盘图标
function initTray() {
    tray = new electron.Tray(path.join(__dirname, 'img/favicon.png'))
    const contextMenu = electron.Menu.buildFromTemplate([{
        label: '退出',
        click: function () {
            electron.app.quit();
        }
    }])
    tray.setContextMenu(contextMenu)
    tray.setTitle('tray')
    tray.setToolTip('托盘图标')
}



async function openWindow() {
    await screenshot({
        filename: __dirname + '/temp.png'
    })
    let win = new electron.BrowserWindow({
        alwaysOnTop: true,
        width: 643,
        height: 487,
        x: 1920,
        y: 0,
        webPreferences: {
            nodeIntegration: true
        },
        autoHideMenuBar: true
    })
    win.loadFile('index.html')
    electron.app.on('window-all-closed',event=>{
        //取消默认退出行为
    })
}


electron.app.on('ready', function () {
    electron.globalShortcut.register('ctrl+shift+q', openWindow)
    initTray()
});